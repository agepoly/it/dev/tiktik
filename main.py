from io import BytesIO
import logging


from PIL import ImageFont
from telegram import InlineKeyboardButton, InlineKeyboardMarkup, Update
from telegram.error import BadRequest
from telegram.ext import CallbackContext, CallbackQueryHandler, CommandHandler, ConversationHandler, Filters, MessageHandler, Updater
from usb.core import NoBackendError
import brotherlabel


from items import Text
from label import Label
from secret import HELP_URL, TOKEN, USB_BACKEND


# Enable logging
logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.INFO
)

logger = logging.getLogger(__name__)

CONFIG = {}
POSITIONS = ["center", "top", "bottom"]
FONTS = {
    "arial": "fonts/arialbi.ttf",
    "roboto": "fonts/roboto.ttf",
    "dancing": "fonts/dancing.ttf",
    "agepoly": "fonts/agepoly.ttf",
}
TAPES = {
    "3.5": brotherlabel.Tape.TZe3_5mm,
    "6": brotherlabel.Tape.TZe6mm,
    "9": brotherlabel.Tape.TZe9mm,
    "12": brotherlabel.Tape.TZe12mm,
    "18": brotherlabel.Tape.TZe18mm,
    "24": brotherlabel.Tape.TZe24mm,
    "36": brotherlabel.Tape.TZe36mm,
}


def set_factory():
    font = ImageFont.truetype(FONTS[CONFIG["font"]["name"]], CONFIG["font"]["size"])
    CONFIG["factory"] = Label(Text(font, pad_right=5, pad_left=5))


def init_config():
    CONFIG["debug"] = None
    CONFIG["tape"] = "12"
    CONFIG["height"] = 100
    CONFIG["ratio"] = 1  # [0.2;1]
    CONFIG["border"] = False
    CONFIG["position"] = "center"
    CONFIG["font"] = {}
    CONFIG["font"]["name"] = "agepoly"
    CONFIG["font"]["size"] = 120
    set_factory()


def init_printer():
    try:
        backend = brotherlabel.USBBackend(USB_BACKEND)
        CONFIG["printer"] = brotherlabel.PTPrinter(backend)
        CONFIG["printer"].tape = TAPES[CONFIG["tape"]]
        CONFIG["printer"].quality = brotherlabel.Quality.high_quality
        CONFIG["printer"].margin = 0
        CONFIG["height"] = CONFIG["printer"].tape.value["print_area"]
    except (ValueError, NoBackendError) as e:
        logger.warning(
            "No backend available. Did you plug the printer in? Did you input the correct backend?"
        )
        CONFIG["debug"] = "[Erreur] Pas d'imprimante détectée. Est ce que l'imprimante est allumée ?"
        


def render_image(text=None):
    if text:
        CONFIG["factory"].set_text(text)

    img = CONFIG["factory"].render(
        ratio=CONFIG["ratio"],
        border=CONFIG["border"],
        start=CONFIG["position"],
        height=CONFIG["printer"].tape.value["print_area"],
    )

    return img


PREVIEW = range(1)
MENU, CONF, ACCOUNT, ADMIN = range(4)


def start(update: Update, context: CallbackContext):
    update.message.reply_text(
        f"🖨 Hey ! Bienvenue sur le bot TikTik !\n"
        f"- Pour imprimer une étiquette, tu peux directement envoyer ton texte, et suivre les instructions.\n"
        f"- Pour régler ton impression utilise /conf."
    )


def menu(update: Update, context: CallbackContext):
    query = update.callback_query
    if query:
        query.answer()

    keyboard = [
        [InlineKeyboardButton("⚙️ Configuration", callback_data="conf")],
        # # Future feature!
        # [InlineKeyboardButton("🧑 Mon Compte", callback_data="account")],
    ]

    # # Future feature!
    # is_admin = False
    # if is_admin or CONFIG["debug"]:
    #     keyboard.append([InlineKeyboardButton("💰 Administration", callback_data="admin")])

    reply_markup = InlineKeyboardMarkup(keyboard)

    text = "🔘 Menu\n"
    text += "Clique sur Configuration pour les paramètres d'impression.\n"
    # # Future feature!
    # text += "Clique sur Mon Compte pour voir tes crédits d'impression.\n"
    # if is_admin or CONFIG["debug"]:
    #     text += "Clique sur Administration pour gérer le bot et les crédits.\n"
    # text += "\nPour imprimer une étiquette, tu peux directement envoyer ton texte, et suivre les instructions.\n"

    if query:
        query.edit_message_text(text, reply_markup=reply_markup)
    else:
        update.message.reply_text(text, reply_markup=reply_markup)

    return MENU


def conf(update: Update, context: CallbackContext):
    query = update.callback_query
    if query:
        query.answer()

        if query.data == "set_font":
            fonts = list(FONTS.keys())
            CONFIG["font"]["name"] = fonts[(fonts.index(CONFIG["font"]["name"]) + 1) % len(fonts)]
            set_factory()
        elif query.data == "set_border":
            CONFIG["border"] = not CONFIG["border"]
        elif query.data.startswith("set_tape:"):
            CONFIG["tape"] = query.data.split(":")[1]
            CONFIG["printer"].tape = TAPES[CONFIG["tape"]]
        elif query.data.startswith("set_ratio:"):
            CONFIG["ratio"] = float(query.data.split(":")[1])
        elif query.data == "set_position":
            CONFIG["position"] = POSITIONS[(POSITIONS.index(CONFIG["position"]) + 1) % len(POSITIONS)]

    keyboard = [
        [
            InlineKeyboardButton(f"Police: {CONFIG['font']['name']}", callback_data="set_font"),
            InlineKeyboardButton(
                f"Bordure: {'' if CONFIG['border'] else 'dés'}activée", callback_data="set_border"
            ),
        ],
        [
            InlineKeyboardButton(f"Tape: {CONFIG['tape']}mm", callback_data="set_tape"),
            InlineKeyboardButton(f"Ratio: {CONFIG['ratio']}", callback_data="set_ratio"),
        ],
        [
            InlineKeyboardButton(f"Position: {CONFIG['position']}", callback_data="set_position"),
            InlineKeyboardButton("↩️ Retour", callback_data="menu"),
        ],
    ]
    reply_markup = InlineKeyboardMarkup(keyboard)

    text = "⚙️ Configuration\n"
    text += "Attention, la configuration est globale pour l'imprimante, et donc pour tous les utilisateur·trice·s !"

    if query:
        query.edit_message_text(text, reply_markup=reply_markup)
    else:
        update.message.reply_text(text, reply_markup=reply_markup)


    return CONF


def set_tape(update: Update, context: CallbackContext):
    query = update.callback_query
    query.answer()

    keyboard = []
    for tape in TAPES.keys():
        keyboard.append(InlineKeyboardButton(tape, callback_data=f"set_tape:{tape}"))

    keyboard = [
        keyboard[: 1 + len(keyboard) // 2],
        keyboard[1 + len(keyboard) // 2 :] + [InlineKeyboardButton("↩", callback_data="conf")],
    ]
    reply_markup = InlineKeyboardMarkup(keyboard)

    text = "⚙️ Choix de la tape\n"
    text += "Attention, bien vérifier d'avoir changé la tape avant de sélectionner une nouvelle valeur ici !"

    query.edit_message_text(text, reply_markup=reply_markup)

    return CONF


def set_ratio(update: Update, context: CallbackContext):
    query = update.callback_query
    query.answer()

    keyboard = []
    for ratio in range(2, 11):
        keyboard.append(
            InlineKeyboardButton(str(ratio / 10), callback_data=f"set_ratio:{ratio / 10}")
        )

    keyboard = [
        keyboard[: 1 + len(keyboard) // 2],
        keyboard[1 + len(keyboard) // 2 :] + [InlineKeyboardButton("↩", callback_data="conf")],
    ]
    reply_markup = InlineKeyboardMarkup(keyboard)

    text = "⚙️ Choix du ratio\n"

    query.edit_message_text(text, reply_markup=reply_markup)

    return CONF


def account(update: Update, context: CallbackContext):
    query = update.callback_query
    query.answer()

    keyboard = [
        [
            InlineKeyboardButton("↩️ Retour", callback_data="menu"),
        ]
    ]
    reply_markup = InlineKeyboardMarkup(keyboard)

    text = "🧑 Mon Compte\n"
    text += "Pour l'instant, il n'y a rien ici (:."

    query.edit_message_text(text, reply_markup=reply_markup)

    return ACCOUNT


def admin(update: Update, context: CallbackContext):
    query = update.callback_query
    query.answer()

    keyboard = [
        [
            InlineKeyboardButton("↩️ Retour", callback_data="menu"),
        ]
    ]
    reply_markup = InlineKeyboardMarkup(keyboard)

    text = "💰 Administration\n"
    text += "Pour l'instant, il n'y a rien ici (:."

    query.edit_message_text(text, reply_markup=reply_markup)

    return ADMIN


def preview(update: Update, context: CallbackContext):
    text = update.message.text

    img = render_image(text)

    output = BytesIO()
    img.save(output, format="png")
    img_str = output.getvalue()

    keyboard = [
        [
            InlineKeyboardButton("❌ Annuler", callback_data="cancel"),
            InlineKeyboardButton("🖨️ Imprimer", callback_data="print"),
        ]
    ]
    reply_markup = InlineKeyboardMarkup(keyboard)

    try:
        update.message.reply_photo(
            img_str, caption="Preview de l'impression", reply_markup=reply_markup
        )
    except BadRequest:
        update.message.reply_text(
            "Preview pas possible sur un texte trop long.", reply_markup=reply_markup
        )

    return PREVIEW


def print_text(update: Update, context: CallbackContext):
    try:
        CONFIG["printer"].get_status().to_string()
    except:
        init_printer()
    
    img = render_image()

    query = update.callback_query
    query.answer()
    m = query.edit_message_caption("Impression lancée.")

    if CONFIG["debug"]:
        m.reply_text(CONFIG["debug"])
        CONFIG["debug"] = None
    else:
        CONFIG["printer"].print([img])

    return ConversationHandler.END


def stop(update: Update, context: CallbackContext):
    query = update.callback_query
    query.answer()
    query.edit_message_caption("Impression annulée.")

    return ConversationHandler.END


def main() -> None:
    updater = Updater(token=TOKEN, use_context=True)
    dispatcher = updater.dispatcher

    menu_handler = ConversationHandler(
        entry_points=[CommandHandler(["menu"], menu)],
        states={
            MENU: [
                CallbackQueryHandler(conf, pattern="^conf$"),
                CallbackQueryHandler(account, pattern="^account$"),
                CallbackQueryHandler(admin, pattern="^admin$"),
            ],
            CONF: [
                CallbackQueryHandler(conf, pattern="^set_font$"),
                CallbackQueryHandler(set_tape, pattern="^set_tape$"),
                CallbackQueryHandler(conf, pattern="^set_tape:"),
                CallbackQueryHandler(conf, pattern="^set_border$"),
                CallbackQueryHandler(set_ratio, pattern="^set_ratio$"),
                CallbackQueryHandler(conf, pattern="^set_ratio:"),
                CallbackQueryHandler(conf, pattern="^set_position$"),
                CallbackQueryHandler(conf, pattern="^conf$"),
                CallbackQueryHandler(menu, pattern="^menu$"),
            ],
            ACCOUNT: [
                CallbackQueryHandler(menu, pattern="^menu$"),
            ],
            ADMIN: [
                CallbackQueryHandler(menu, pattern="^menu$"),
            ],
        },
        fallbacks=[CommandHandler(["menu"], menu)],
    )
    
    conf_handler = ConversationHandler(
        entry_points=[CommandHandler(["conf"], conf)],
        states={
            CONF: [
                CallbackQueryHandler(conf, pattern="^set_font$"),
                CallbackQueryHandler(set_tape, pattern="^set_tape$"),
                CallbackQueryHandler(conf, pattern="^set_tape:"),
                CallbackQueryHandler(conf, pattern="^set_border$"),
                CallbackQueryHandler(set_ratio, pattern="^set_ratio$"),
                CallbackQueryHandler(conf, pattern="^set_ratio:"),
                CallbackQueryHandler(conf, pattern="^set_position$"),
                CallbackQueryHandler(conf, pattern="^conf$"),
                CallbackQueryHandler(menu, pattern="^menu$"),
            ],
        },
        fallbacks=[CommandHandler(["conf"], conf)],
    )

    preview_handler = ConversationHandler(
        entry_points=[MessageHandler(Filters.text & ~Filters.command, preview)],
        states={
            PREVIEW: [
                CallbackQueryHandler(print_text, pattern="^print$"),
                CallbackQueryHandler(stop, pattern="^cancel$"),
                MessageHandler(Filters.text & ~Filters.command, preview),
            ],
        },
        fallbacks=[CommandHandler(["cancel", "stop"], stop)],
    )

    dispatcher.add_handler(CommandHandler(["start"], start)) 
    dispatcher.add_handler(conf_handler)
    dispatcher.add_handler(menu_handler)
    dispatcher.add_handler(preview_handler)

    init_config()
    init_printer()

    updater.start_polling()
    updater.idle()


if __name__ == "__main__":
    main()
