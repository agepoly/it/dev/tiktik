from io import BytesIO
import logging
import math
import os
import pprint


from PIL import Image, ImageFont
from telegram.ext import CommandHandler, Filters, MessageHandler, Updater
from usb.core import NoBackendError
import brotherlabel
import qrcode


from items import Text
from label import Label
from secret import HELP_URL, TOKEN, USB_BACKEND


# Enable logging
logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.INFO
)


def error(update, context):
    """
    Error handler
    """
    logger.warning(f'Update "{update}" caused error "{context.error}"')


logger = logging.getLogger(__name__)

CONFIG = {}
POSITIONS = ["center", "top", "bottom"]
FONTS = ["arial", "roboto", "dancing", "agepoly"]
TAPES = {
    "3.5": brotherlabel.Tape.TZe3_5mm,
    "6": brotherlabel.Tape.TZe6mm,
    "9": brotherlabel.Tape.TZe9mm,
    "12": brotherlabel.Tape.TZe12mm,
    "18": brotherlabel.Tape.TZe18mm,
    "24": brotherlabel.Tape.TZe24mm,
    "36": brotherlabel.Tape.TZe36mm,
}


def set_factory():
    font = ImageFont.truetype(os.path.join("fonts", CONFIG["font"]["name"]), CONFIG["font"]["size"])
    CONFIG["factory"] = Label(Text(font, pad_right=5, pad_left=5))


def init_config():
    CONFIG["debug"] = False
    CONFIG["tape"] = "12"
    CONFIG["ratio"] = 1  # [0.2;1]
    CONFIG["border"] = False
    CONFIG["position"] = "center"
    CONFIG["font"] = {}
    CONFIG["font"]["name"] = "agepoly.ttf"
    CONFIG["font"]["size"] = 120
    set_factory()


def init_printer():
    try:
        backend = brotherlabel.USBBackend(USB_BACKEND)
        CONFIG["printer"] = brotherlabel.PTPrinter(backend)
        CONFIG["printer"].tape = TAPES[CONFIG["tape"]]
        CONFIG["printer"].quality = brotherlabel.Quality.high_quality
        CONFIG["printer"].margin = 0
        CONFIG["height"] = CONFIG["printer"].tape.value["print_area"]
    except NoBackendError as e:
        logger.warning(
            "No backend available. Did you plug the printer in? Did you input the correct backend?"
        )
        CONFIG["debug"] = True


def render_image(text=None):
    if text:
        CONFIG["factory"].set_text(text)

    img = CONFIG["factory"].render(
        ratio=CONFIG["ratio"],
        border=CONFIG["border"],
        start=CONFIG["position"],
        height=CONFIG["height"],
    )

    return img


def start(update, context):
    update.message.reply_text(
        f"Hey! Bienvenue sur le bot TikTik ! Si tu as besoin de plus d'informations: {HELP_URL}"
    )


def printPDF(update, context):
    print("Hello")
    context.bot.get_file(update.message.document).download(custom_path="outTemp.pdf")
    os.system("/usr/bin/lpr -P ageplp2_epfl_ch outTemp.pdf")
    os.remove("outTemp.pdf")


def show_status(update, context):
    status = CONFIG["printer"].get_status().to_string()
    update.message.reply_text(status)
    logger.info(status)


def set_font(update, context):
    message = " ".join(context.args)

    if message == "arial":
        CONFIG["font"]["name"] = "fonts/arialbi.ttf"
    elif message == "roboto":
        CONFIG["font"]["name"] = "fonts/roboto.ttf"
    elif message == "dancing":
        CONFIG["font"]["name"] = "fonts/dancing.ttf"
    elif message == "agepoly":
        CONFIG["font"]["name"] = "fonts/agepoly.ttf"
    else:
        CONFIG["font"]["name"] = "fonts/arialbi.ttf"

    set_factory()

    update.message.reply_text(f"Police définie sur \"{CONFIG['font']['name']}\".")


def set_ratio(update, context):
    try:
        ratio = float(" ".join(context.args))
    except ValueError:
        update.message.reply_text("Merci d'utiliser un nombre entre 0.2 et 1.")
        return

    if ratio < 0.2:
        CONFIG["ratio"] = 0.2
    elif ratio > 1:
        CONFIG["ratio"] = 1
    else:
        CONFIG["ratio"] = ratio

    update.message.reply_text(f"Ratio défini sur {CONFIG['ratio']}.")


def set_border(update, context):
    border = " ".join(context.args).lower()

    if border in ["oui", "yes", "true", "ui", "1"]:
        CONFIG["border"] = True
    elif border in ["non", "no", "false", "nope", "0"]:
        CONFIG["border"] = False
    else:
        CONFIG["border"] = not CONFIG["border"]

    update.message.reply_text(f"Bordure {'' if CONFIG['border'] else 'dés'}activée.")


def set_position(update, context):
    position = " ".join(context.args).lower()

    if position in ["top", "haut"]:
        CONFIG["position"] = "top"
    elif position in ["center", "centre"]:
        CONFIG["position"] = "center"
    elif position in ["bottom", "bas"]:
        CONFIG["position"] = "bottom"
    else:
        update.message.reply_text(f"Choix possibles : {', '.join(POSITIONS)}.")
        return

    update.message.reply_text(f"Position définie sur \"{CONFIG['position']}\".")


def set_tape(update, context):
    tape = " ".join(context.args)

    if tape not in TAPES.keys():
        update.message.reply_text(f"Choix possibles : {', '.join(TAPES.keys())}.")
        return

    CONFIG["tape"] = tape
    init_printer()

    update.message.reply_text(f"Tape définie sur {CONFIG['tape']}mm.")


def show_config(update, context):
    debug = " ".join(context.args) or CONFIG["debug"]

    if debug:
        text = pprint.pformat(CONFIG, indent=2)
        logger.info(text)
    else:
        text = ""
        text += f"Police : {CONFIG['font']['name']}\n"
        text += f"Tape : {CONFIG['tape']}\n"
        text += f"Ratio : {CONFIG['ratio']}\n"
        text += f"Bordure : {'' if CONFIG['border'] else 'dés'}activée\n"
        text += f"Position : {CONFIG['position']}\n"

    update.message.reply_text(text)


def display_text(update, context):
    text = " ".join(context.args)

    if not text:
        update.message.reply_text("Merci de donner un texte pour impression.")
        return

    img = render_image(text)

    output = BytesIO()
    img.save(output, format="png")
    img_str = output.getvalue()

    update.message.reply_text(f"Texte demandé : {text}").reply_photo(
        img_str, caption="Preview", quote=True
    )


def print_text(update, context):
    try:
        CONFIG["printer"].get_status().to_string()
    except:
        init_printer()

    text = " ".join(context.args)

    if not text:
        update.message.reply_text("Merci de donner un texte pour impression.")
        return

    img = render_image(text)

    m = update.message.reply_text(f"Texte demandé : {text}")

    CONFIG["printer"].print([img])

    m.reply_text("Impression en cours...")


def make_qr(update, context):
    # TODO
    try:
        CONFIG["printer"].get_status().to_string()
    except:
        init_printer()

    text = " ".join(context.args)

    if not text:
        update.message.reply_text("Merci de donner un texte pour impression.")
        return

    # Generate qrcode and resize
    img = qrcode.make(text)
    img = img.resize((454, 454))

    m = update.message.reply_text(f"Texte demandé : {text}")

    printer.print([img])

    m.reply_text("Impression en cours...")


def receive_images(update, context):
    # TODO
    # Download the image and double the size
    downloaded_img = Image.open(
        BytesIO(context.bot.getFile(update.message.photo[-1].file_id).download_as_bytearray())
    )
    decode_img = downloaded_img.resize((downloaded_img.width * 2, downloaded_img.height * 2))

    # Confirm printing, split image by height strips and print
    context.bot.sendMessage(update.effective_chat.id, "Preparing scissors and glue...")
    for i in range(0, math.ceil(decode_img.height / height)):
        printer.print(
            [
                decode_img.crop(
                    (0, height * i, decode_img.width, min(height * (i + 1), decode_img.height))
                ).convert("1")
            ]
        )


def main():
    updater = Updater(token=TOKEN, use_context=True)

    dispatcher = updater.dispatcher

    dispatcher.add_handler(CommandHandler("start", start))
    # dispatcher.add_handler(CommandHandler("qr", make_qr))
    dispatcher.add_handler(CommandHandler("print", print_text))
    dispatcher.add_handler(CommandHandler("display", display_text))
    # dispatcher.add_handler(MessageHandler(Filters.photo, receive_images))
    # dispatcher.add_handler(MessageHandler(Filters.document.mime_type("application/pdf"), printPDF))

    dispatcher.add_handler(CommandHandler("font", set_font))
    dispatcher.add_handler(CommandHandler("tape", set_tape))
    dispatcher.add_handler(CommandHandler("ratio", set_ratio))
    dispatcher.add_handler(CommandHandler("border", set_border))
    dispatcher.add_handler(CommandHandler("position", set_position))
    dispatcher.add_handler(CommandHandler("config", show_config))
    dispatcher.add_handler(CommandHandler("status", show_status))

    dispatcher.add_error_handler(error)

    init_config()
    init_printer()

    updater.start_polling()

    updater.idle()


if __name__ == "__main__":
    main()
