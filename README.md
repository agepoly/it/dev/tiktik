# Tiktik :printer:

A Telegram bot developed to receive requests to print text, images and QR-codes on a Brother P9000W labeling machine.

## Example of image being printed

Requested image:

![demo1](./img/demo1.jpg)

Printed image:

![demo2](./img/demo2.jpg)

## How to use
### Available directly

You can use [@TikTik_AgepBot](https://t.me/TikTik_AgepBot) to test the bot and use it directly.

> Note: You need to turn on the printer first.

To print, directly send your text, then press "Imprimer".

### Self-hosting

1. Need Python 3.7+, python3-venv, and Pip 21+.
2. Clone the repo, `cd` into it.
3. `python -m venv ./env; source ./env/bin/activate`
4. `pip install -r requirements.txt`
5. `cp secret.dist.py secret.py` and
   - `TOKEN`: [Your @BotFather token](https://core.telegram.org/bots).
   - `USB_BACKEND`: URI to the backend (see [How to find the backend](#how-to-find-the-backend)).
   - `HELP_URL`: Link to the help article.
6. `./main.py` or `python ./main.py`

If you intend to push modifications, please activate pre-commit: `pre-commit install`.

## Setup
### Raspberry and the TikTik bot

If it does not work for whatever reason:
* Shut down everything.
* Start the printer first, then the Raspberry.
* Wait a few secondes, and try again.

If it still does work, check if the printer has a correct configuration (e.g., it's the correct cartouche in the printer).

### How to find the backend

* Backend corresponds roughly to how the program can connect to the printer.
* Backend address shall be something like `usb://0x1234:0x5678/123456789...`:
  * `XXXX` is the idVendor.
  * `YYYY` is the idProduct.
  * `ABCDETGHI...` is the serial number of the product.
* To find your backend, you must connect the printer to your computer.

> Note: The serial number is not compulsory for the backend to work.

#### Windows

* CLI:
  * Open PowerShell, and type `Get-PnpDevice -PresentOnly | Where-Object { $_.InstanceId -match '^USB' } | Select-Object -Property FriendlyName, InstanceId`.
  * Then, refer to "Both of them".
* GUI:
  * Go to the Device manage.
  * Go under Printers and select your printer (in our case, Brother PT-P9000W).
  * Left-clic, properties, then the "Events" (Événéments) tab.
  * Then, refer to "Both of them".
* Both of them:
  * Next to a description of a printer (e.g., "Printer" or "Prise en charge d'impression" or whatnot), you'll find something looking like `USB\VID_XXXX&PID_YYYY\ABCDEFGHI...`, e.g., `USB\VID_1234&PID_5678\123456789...`, which corresponds to the InstanceId.
  * You have to use as backend `usb://0xXXXX:0xYYYY/ABCDEFGHI...` e.g., `usb://0x1234:0x5678/123456789...`

> Note: You may need to install the corresponding [driver](https://support.brother.com/g/b/downloadlist.aspx?c=fr&lang=fr&prod=p900weuk&os=10011) and [library](https://stackoverflow.com/a/31332121).

> Note: [P-touch Editor5.x](https://support.brother.com/g/b/downloadend.aspx?c=fr&lang=fr&prod=p900weuk&os=10011&dlid=dlfp100990_000&flang=185&type3=296) is a cool alternative if you _just_ need to _print_ on Windows.

#### Linux

* haha [lsusb](https://linux.die.net/man/8/lsusb) go brr.
* Basically, type `lsudb -v`, find your printer (in our case, Brother PT-P9000W), and format your finding according to the general information.
* If it does not want, you may want to install `usbutils`.

## Credits

* This project is a mix of [labelprinterkit](https://github.com/NotAFile/labelprinterkit) and [py-brotherlabel](https://github.com/masatomizuta/py-brotherlabel)
* Also go check [pyusb](https://github.com/pyusb/pyusb), it's pretty cool.
