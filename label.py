"""
Labels are the Base class you derive your Labels from. A few simple Labels are
provided for you.
"""
from typing import Tuple


from PIL import Image, ImageChops, ImageDraw


def _coord_add(tup1, tup2):
    """add two tuples of size two"""
    return tup1[0] + tup2[0], tup1[1] + tup2[1]


class Label:
    """Base class for all labels"""

    def __init__(self, items):
        self._rendered_items = None

        if not items:
            raise ValueError("A Label class must contain a list of rendarable objects.")

        if not isinstance(items, list):
            items = [items]

        self.items = [items]

    def set_text(self, *args):
        arg_it = iter(args)
        try:
            self._rendered_items = [
                [item.render(next(arg_it)) for item in line] for line in self.items
            ]
        except StopIteration:
            # the argument list was exhausted before all items had a value
            raise TypeError(
                "{cls} requires {argc} arguments, but {num} were given".format(
                    cls=self.__class__.__name__, argc=sum(len(x) for x in self.items), num=len(args)
                )
            )

    @property
    def size(self) -> Tuple[int, int]:
        width = max(sum(i.size[0] for i in line) for line in self._rendered_items)
        height = sum(max(i.size[1] for i in line) for line in self._rendered_items)

        return width, height

    def render(self, ratio=1, border=False, start="center", height=None) -> Image:
        """render the Label.

        Args:
            width: Width request
            height: Height request
        """
        size = self.size
        size = (round(size[0] * ratio), size[1])
        img = Image.new("1", size, "white")

        pos = [0, 0]
        x_start = 0

        if start == "center":
            pos_start = round(size[1] * ((1 - ratio) / 2))
        elif start == "top":
            pos_start = 0
            x_start = 0
            if ratio <= 0.9:
                pos_start = pos_start + 4
                x_start = -round(size[1] * ((1 - ratio) / 2)) + 4
        elif start == "bottom":
            pos_start = round((size[1] - size[1] * ratio))
            if ratio <= 0.9:
                pos_start = pos_start - 4
                x_start = round(size[1] * ((1 - ratio) / 2)) - 4
        else:
            raise ValueError("Wrong position")

        for line in self._rendered_items:
            for item in line:
                item_width, item_height = item.size
                item = item.resize((round(item_width * ratio), round(item_height * ratio)))

                img.paste(item, (0, pos_start))
                pos[0] += item.size[0]

            pos[0] = 0
            pos[1] += max(i.size[1] for i in line)

        if border and ratio <= 0.9:
            draw = ImageDraw.Draw(img)

            diff_x = 2 - (size[1] * ratio - size[1] * 0.9) / 2
            diff_y = 2
            radius = 10 - 9 * (0.9 - ratio)

            # Top
            draw.line(
                [
                    (diff_y + radius, diff_x + x_start),
                    (size[0] - diff_y - radius, diff_x + x_start),
                ],
                fill="black",
                width=1,
            )
            # Right
            draw.line(
                [
                    (size[0] - diff_y, diff_x + x_start + radius),
                    (size[0] - diff_y, size[1] - diff_x - radius + x_start),
                ],
                fill="black",
                width=1,
            )
            # Bottom
            draw.line(
                [
                    (diff_y + radius, size[1] + x_start - diff_x),
                    (size[0] - diff_y - radius, size[1] - diff_x + x_start),
                ],
                fill="black",
                width=1,
            )
            # Left
            draw.line(
                [
                    (diff_y, diff_x + x_start + radius),
                    (diff_y, size[1] - diff_x + x_start - radius),
                ],
                fill="black",
                width=1,
            )

            # Top left
            draw.arc(
                [(diff_y, diff_x + x_start), (diff_y + 2 * radius, diff_x + x_start + 2 * radius)],
                180,
                270,
                fill="black",
                width=1,
            )
            # Bottom left
            draw.arc(
                [
                    (diff_y, size[1] - diff_x - 2 * radius + x_start),
                    (diff_y + 2 * radius, size[1] - diff_x + x_start),
                ],
                90,
                180,
                fill="black",
                width=1,
            )
            # Top right
            draw.arc(
                [
                    (size[0] - diff_y - 2 * radius, diff_x + x_start),
                    (size[0] - diff_y, diff_x + 2 * radius + x_start),
                ],
                270,
                359,
                fill="black",
                width=1,
            )
            # Bottom right
            draw.arc(
                [
                    (size[0] - diff_y - 2 * radius, size[1] - diff_x - 2 * radius + x_start),
                    (size[0] - diff_y, size[1] - diff_x + x_start),
                ],
                0,
                90,
                fill="black",
                width=1,
            )

        x_dim, y_dim = img.size
        x_dim = x_dim
        y_dim = y_dim

        height = round(height)
        x_dim = round((height / y_dim) * x_dim)

        img = img.resize((x_dim, height))

        return img
